<?php

namespace App\Tests\Core;

use App\Core\Card;
use App\Core\Guess;
use PHPUnit\Framework\TestCase;

class GuessTest extends TestCase
{

    public function testCardsValidGame()
    {
        $guess = new Guess();
        $this->assertEquals(52, $guess->getCards()->count());
    }

    public function testCardsInvalidGame()
    {
        $guess = new Guess(22);
        $this->assertEquals(52, $guess->getCards()->count());
    }

    public function testHelpIsFalse()
    {
        $guess = new Guess();
        $this->assertFalse($guess->isHelp());
    }

    public function testHelpIsTrue()
    {
        $guess = new Guess(52, true);
        $this->assertTrue($guess->isHelp());
    }

    public function testSelectedCard()
    {
        // DefaultCardForHelp
        $guess = new Guess(52, false, true);
        $defaultCardForHelp = new Card("roi", "pique");
        $this->assertEquals(0, Card::compare($guess->getSelectedCard(), $defaultCardForHelp));
    }

    public function testVerifyWithHelp()
    {
        // carte par défault pour test => roi de pique
        $guess = new Guess(52, true, true);

        // -1 si la carte qu'il a soumise est plus petite que celle à deviner.
        $this->assertEquals(-1, $guess->verify(new Card("2", "coeur")));

        // +1 si la carte qu'il a soumise est plus grande que celle à deviner.
        $this->assertEquals(+1, $guess->verify(new Card("as", "pique")));
    }

    public function testVerifyWithoutHelp()
    {
        // carte par défault pour test => roi de pique

        $guess = new Guess(52, false, true);

        // O si ce n'est pas la bonne carte
        $this->assertEquals(0, $guess->verify(new Card("3", "trèfle")));

        // 1 si c'est la bonne carte
        $this->assertEquals(1, $guess->verify(new Card("roi", "pique")));
    }

    public function testSubmissions()
    {
        $guess = new Guess();
        $this->assertEquals(0, $guess->getSubmissions());

        $guess->verify(new Card("5", "coeur"));
        $guess->verify(new Card("as", "coeur"));
        $this->assertEquals(2, $guess->getSubmissions());

        for ($i = 0; $i < 6; $i++) {
            $guess->verify(new Card("4", "coeur"));
        }
        $this->assertEquals(8, $guess->getSubmissions());
    }

    public function testStatsDirectFound()
    {
        // carte à deviner == Roi -> Pique
        $guess = new Guess(52, false, true);
        $guess->verify(new Card("roi", "pique"));
        $this->assertEquals("Bravo, vous l'avez trouvé du premier coup, vous êtes chanceux. Nombre de soumission(s) : 1", $guess->stats());
    }


    public function testStatsWithGoodStrategy()
    {
        // carte à deviner == Roi -> Pique
        $guess = new Guess(52, false, true);
        $guess->verify(new Card("As", "carreau"));
        $guess->verify(new Card("As", "coeur"));
        $guess->verify(new Card("As", "trèfle"));
        $guess->verify(new Card("As", "pique"));
        $guess->verify(new Card("roi", "carreau"));
        $guess->verify(new Card("roi", "coeur"));
        $guess->verify(new Card("roi", "trèfle"));
        $guess->verify(new Card("roi", "pique"));
        $this->assertEquals("Bravo, vous avez trouvé en testant les cartes par nom/couleur, une bonne stratégie. Nombre de soumission(s) : 8", $guess->stats());
    }

    public function testStatsWithBadStrategy()
    {
        // carte à deviner == Roi -> Pique
        $guess = new Guess(52, false, true);
        $guess->verify(new Card("3", "trèfle"));
        $guess->verify(new Card("4", "carreau"));
        $guess->verify(new Card("roi", "pique"));
        $guess->verify(new Card("8", "trèfle"));
        $guess->verify(new Card("6", "trèfle"));
        $guess->verify(new Card("roi", "pique"));
        $this->assertEquals("Bravo, mais vous êtes chanceux, car vous avez trouvé en testant les cartes un peu aléatoirement. Nombre de soumission(s) : 6", $guess->stats());
    }

    public function testStatsMoreThan52Trials()
    {
        // carte à deviner == Roi -> Pique
        $guess = new Guess(52, false, true);

        for ($i = 0; $i < 52; $i++) {
            $guess->verify(new Card("4", "coeur"));
        }
        $guess->verify(new Card("roi", "pique"));

        $this->assertEquals("Bravo, vous avez trouvé. Mais vous effectuez plus d'essais que de carte dans le jeu. Certaines cartes ont été testées plusieurs fois. Nombre de soumission(s) : 53", $guess->stats());
    }

}
