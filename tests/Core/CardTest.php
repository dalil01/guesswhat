<?php

namespace App\Tests\Core;

use PHPUnit\Framework\TestCase;
use App\Core\Card;

class CardTest extends TestCase
{

    public function testName()
    {
        $card = new Card('AS', 'Trèfle');
        $this->assertEquals('as', $card->getName());
    }

    public function testColor()
    {
        $card = new Card('As', 'Trèfle');
        $this->assertEquals('trèfle', $card->getColor());
    }

    public function testToString()
    {
        $o1 = new Card('As', 'pique');
        $this->assertEquals("as de pique", $o1->toString());
    }

    public function testCompareEqualityOfCards()
    {
        // 0 si les deux cartes sont considérées comme égales
        $o1 = new Card('As', 'Trèfle');
        $o2 = new Card('as', 'Trèfle');
        $this->assertEquals(0, Card::compare($o1, $o2));
    }

    public function testCompareSecondSuperiorToFirst()
    {
        // -1 si $o1 est considérée inférieur à $o2
        $o1 = new Card('3', 'pique');
        $o2 = new Card('as', 'coeur');
        $this->assertEquals(-1, Card::compare($o1, $o2));

        $o1 = new Card('as', 'trèfle');
        $o2 = new Card('as', 'pique');
        $this->assertEquals(-1, Card::compare($o1, $o2));
    }

    public function testCompareFirstSuperiorToSecond()
    {
        // +1 si $o1 est considérée supérieur à $o2
        $o1 = new Card('roi', 'pique');
        $o2 = new Card('valet', 'coeur');
        $this->assertEquals(+1, Card::compare($o1, $o2));

        $o1 = new Card('3', 'pique');
        $o2 = new Card('3', 'coeur');
        $this->assertEquals(+1, Card::compare($o1, $o2));
    }

    public function testGameOf52CardsNumbersCards()
    {
        // Nombre total de cartes
        $this->assertEquals(52, Card::gameOf52Cards()->count());
    }

    public function testGameOf52CardsFirstCard()
    {
        // verifie la premiere carte
        $testFirstCard = new Card('as', 'pique');
        $this->assertEquals(0, card::compare(Card::gameOf52Cards()->first(), $testFirstCard));
    }

    public function testGameOf52CardsLastCard()
    {
        // verifie la dernière carte
        $testLastCard = new Card('2', 'trèfle');
        $this->assertEquals(0, card::compare(Card::gameOf52Cards()->last(), $testLastCard));
    }

    public function testSort()
    {
        $cards = [];
        $card = new Card('As', 'Trèfle');
        $cards[] = $card;
        $card = new Card('2', 'Pique');
        $cards[] = $card;

        // vérifie que la première carte est bien un As
        $this->assertEquals('as', $cards[0]->getName());

        // trie le tableau $cards, en utilisant la fonction de comparaison Card::cmp
        // rem : la syntaxe n'est pas intuitive, on doit passer
        // le nom complet de la classe et le nom d'une méthode de comparaison.
        // (voir https://www.php.net/manual/fr/function.usort.php)
        usort($cards, array("App\Core\Card", "compare"));

        // vérifie que le tableau $cards a bien été modifié par usort
        // dans la table ASCII, les chiffres sont placés avant les lettres de l'alphabet
        $this->assertEquals('2', $cards[0]->getName());
    }

}
