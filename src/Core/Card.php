<?php

namespace App\Core;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{

     const CARD_NAMES = ["as" => 12, "roi" => 11, "dame" => 10, "valet" => 9, "10" => 8, "9" => 7, "8" => 6, "7" => 5, "6" => 4, "5" => 3, "4" => 2, "3" => 1, "2" => 0];
     
     const CARD_COLORS = ["pique" => 3, "carreau" => 2, "coeur" => 1, "trèfle" => 0];
     
    /**
     * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
     */
    private $name;
    
    /**
     * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
     */
    private $color;

    /**
     * Card constructor.
     * @param string $name
     * @param string $color
     */
    public function __construct(string $name, string $color)
    {
        $this->name = strtolower($name);
        $this->color = strtolower($color);
    }

    /** définir une relation d'ordre entre instance de Card.
     *  Remarque : cette méthode n'est pas de portée d'instance
     *
     * @see https://www.php.net/manual/fr/function.usort.php
     *
     * @param $o1 Card
     * @param $o2 Card
     * @return int
     *
     *  Ordre de grandeur des cartes
     *  Pique > carreau > Coeur > Trèfle
     *
     * <ul>
     *  <li> 0 si les deux cartes sont considérées comme égales </li>
     *  li> -1 si $o1 est considéré inférieur à $o2</li>
     *  <li> +1 si $o1 est considéré supérieur à $o2</li>
     * </ul>
     *
     */
    public static function compare(Card $o1, Card $o2): int
    {
        $o1Name = $o1->name; $o2Name = $o2->name;
        $o1Color = $o1->color; $o2Color = $o2->color;

        if ($o1Name === $o2Name && $o1Color === $o2Color) {
            return 0;
        }
        
        if (self::CARD_COLORS[$o1Color] >= self::CARD_COLORS[$o2Color]) {
            return (self::CARD_NAMES[$o1Name] >= self::CARD_NAMES[$o2Name]) ? +1 : -1;
        }

        /* Ex :
         * carte 1 == 3 de carreau
         * carte 2 == 3 de pique
         * Une fois ici c'est que la couleur de la carte 1 est forcément < a la couleur de la carte 2
         * Comme les noms sont égaux, la deuxième carte est forcément supérieur , on return -1
         */
         return (self::CARD_NAMES[$o1Name] > self::CARD_NAMES[$o2Name]) ? +1 : -1;
    }

    /**
     * @return ArrayCollection tableau contenant 52 cartes
     */
    public static function gameOf52Cards(): ArrayCollection
    {
        $allCards = new ArrayCollection();

        foreach (self::CARD_COLORS as $ccKey => $ccValue) {
            foreach (self::CARD_NAMES as $cnKey => $cnValue) {
                $allCards->add(new Card($cnKey, $ccKey));
            }
        }
        
        return $allCards;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getColor(): string
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor(string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return string 
     */
    public function toString(): string
    {
        return $this->name . " de " . $this->color;
    }

}