<?php

namespace App\Core;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
    /**
     * @var $cards ArrayCollection a array of Cards
     */
    private $cards;

    /**
     * @var $help bool Si le joueur active l'aide ou non
     */
    private $help;

    /**
     * @var $selectedCard Card This is the card to be guessed by the player
     */
    private $selectedCard;

    /**
     * @var $submissions int nombre de fois où le joueur a soumis une carte
     */
    private $submissions;

    /**
     * @var $verifiedCards ArrayCollection la liste des cartes vérifier
     */
    private $verifiedCards;

    /**
     * Guess constructor.
     * @param int $idGame choix du jeu de cartes
     * @param bool $help Demande d'aide
     * @param bool $defaultCardForTest prendre la carte par défaut pour les tests
     */
    public function __construct(int $idGame = 52, bool $help = false, bool $defaultCardForTest = false)
    {
        $this->cards = ($idGame === 52) ? Card::gameOf52Cards() : Card::gameOf52Cards();
        $this->help = $help;
        $this->selectedCard = ($defaultCardForTest) ? new Card("roi", "pique") : $this->cards->get(mt_rand(0, $this->cards->count() - 1));
        $this->submissions = 0;
        $this->verifiedCards = new ArrayCollection();
    }

    /**
     * @param $card Card la carte soumis
     * @return int
     *
     * O si ce n'est pas la bonne carte
     * 1 si c'est la bonne carte
     *
     * Si l'aide est activé :
     * -1 si la carte qu'il a soumise est plus petite que celle à deviner.
     * +1 si la carte qu'il a soumise est plus grande que celle à deviner.
     */
    public function verify(Card $card): int
    {
        $this->submissions++;
        $this->verifiedCards->add($card);

        if ($this->help) {
            return (Card::compare($card, $this->selectedCard) === 0) ? 1 : Card::compare($card, $this->selectedCard);
        }

        return (Card::compare($card, $this->selectedCard) === 0) ? 1 : 0;
    }

    /**
     * Ps: Les stats s'affichent quand la partie est terminé
     * @return string retourne des éléments d'analyse de la partie
     */
    public function stats(): string
    {
        $strategy = null;

        if ($this->submissions === 1) {
            $strategy = "Bravo, vous l'avez trouvé du premier coup, vous êtes chanceux.";
        }

        if ($this->submissions > 1 && $this->submissions <= $this->cards->count()) {
            $note = 0;
            $recupCard = $this->verifiedCards->first();

            foreach ($this->verifiedCards as $vc) {
                if ($vc->getName() === $recupCard->getName() || $vc->getColor() === $recupCard->getColor()) {
                    $note++;
                } else {
                    $note--;
                }
                $recupCard = $vc;
            }

            $strategy = ($note > 0) ?
                "Bravo, vous avez trouvé en testant les cartes par nom/couleur, une bonne stratégie." :
                "Bravo, mais vous êtes chanceux, car vous avez trouvé en testant les cartes un peu aléatoirement.";
        }

        if ($this->submissions > $this->cards->count()) {
            $strategy = "Bravo, vous avez trouvé. Mais vous effectuez plus d'essais que de carte dans le jeu. Certaines cartes ont été testées plusieurs fois.";
        }

        return $strategy . " Nombre de soumission(s) : " . $this->getSubmissions();
    }

    /**
     * @return ArrayCollection
     */
    public function getCards(): ArrayCollection
    {
        return $this->cards;
    }

    /**
     * @param ArrayCollection $cards
     */
    public function setCards(ArrayCollection $cards): void
    {
        $this->cards = $cards;
    }

    /**
     * @return Card
     */
    public function getSelectedCard(): Card
    {
        return $this->selectedCard;
    }

    /**
     * @param Card $selectedCard
     */
    public function setSelectedCard(Card $selectedCard): void
    {
        $this->selectedCard = $selectedCard;
    }

    /**
     * @return bool
     */
    public function isHelp(): bool
    {
        return $this->help;
    }

    /**
     * @param bool $help
     */
    public function setHelp(bool $help): void
    {
        $this->help = $help;
    }

    /**
     * @return int
     */
    public function getSubmissions(): int
    {
        return $this->submissions;
    }

    /**
     * @param int $submissions
     */
    public function setSubmissions(int $submissions): void
    {
        $this->submissions = $submissions;
    }

    /**
     * @return ArrayCollection
     */
    public function getVerifiedCards(): ArrayCollection
    {
        return $this->verifiedCards;
    }

    /**
     * @param ArrayCollection $verifiedCards
     */
    public function setVerifiedCards(ArrayCollection $verifiedCards): void
    {
        $this->verifiedCards = $verifiedCards;
    }

}

