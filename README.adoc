= GuessWhat

Le projet GuessWhat visait à permettre à ses développeurs de prendre en charge la programmation orientée objet avec PHP mais aussi de découvrir le concept de test unitaire dans la programmation avec PHPUnit (sans interaction avec l'utilisateur). L'objectif était de développer une logique de jeu de cartes.

== Installations nécessaire :

1. PHP 7 ou une version supérieur.
2. Le plugin phpUnit.
3. Composer.


== Présentation de l'équipe :

Ce projet a été développé en pair-programming entre le 04/09/2020 et le 13/09/2020 par Angeles Théo et Chablis Dalil (Etudiant en deuxième année de BTS SIO SLAM).

* Dalil était chargé de développer les tâches les "plus difficiles", d'expliquer et d'aider ses camarades de classe.
* Theo était chargé de développer les tâches les plus "simples" et d'aider dalil à trouver des solutions.

== Les étapes d'un scénario de jeu :

1. (optionnel pour le joueur) paramétrage du jeu (choix du jeu de cartes, aide à la recherche).
2. Lancement d'une partie (le jeu tire une carte "au hasard"), que le joueur doit deviner.
3. Le joueur propose une carte.
4. Si c'est la bonne carte alors la partie se termine et le jeu affiche des éléments d'analyse (la qualité stratégique du joueur, nombre de
 soumission de carte).

* Si ce n'est pas la bonne carte, alors si l'aide est activée, le joueur est informé si la carte qu'il a soumise est
plus petite ou plus grande que celle à deviner. Retour en 3.



== Parlons code...

=== La classe Card (Localisation : `src/Core/Card.php`) :

Cette classe définit la structure d'une carte à jouer.
Ses méthodes sont testées dans `CardTest` (Localisation : `tests/Core/CardTest.php`).

Afin de définir les ordres de grandeurs entre les cartes deux constantes ont été ajoutées :

* CARD_NAME contient un tableau avec le nom des cartes.

* CARD_COLOR contient un tableau avec les différentes couleurs de carte.

[, php]
----

<?php

namespace App\Core;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Card : Définition d'une carte à jouer
 * @package App\Core
 */
class Card
{

   const CARD_NAMES = ["as" => 12, "roi" => 11, "dame" => 10, "valet" => 9, "10" => 8, "9" => 7, "8" => 6, "7" => 5, "6" => 4, "5" => 3, "4" => 2, "3" => 1, "2" => 0];

   const CARD_COLORS = ["pique" => 3, "carreau" => 2, "coeur" => 1, "trèfle" => 0];

  /**
   * @var $name string nom de la carte, comme par exemples 'As' '2' 'Reine'
   */
  private $name;

  /**
   * @var $color string couleur de la carte, par exemples 'Pique', 'Carreau'
   */
  private $color;

  /**
   * Card constructor.
   * @param string $name
   * @param string $color
   */
  public function __construct(string $name, string $color)
  {
      $this->name = strtolower($name);
      $this->color = strtolower($color);
  }

  [...]
----

==== La méthode static `compare`.

Cette méthode permet de comparer deux cartes.

Elle retourne :

* 0 si les deux cartes sont considérées comme égales.
* -1 si la première carte est considérée comme inférieure à la seconde.
* +1 si la première carte est considérée comme supérieur à la deuxième.

[, php]
----
public static function compare(Card $o1, Card $o2): int
{
    $o1Name = $o1->name; $o2Name = $o2->name;
    $o1Color = $o1->color; $o2Color = $o2->color;

    if ($o1Name === $o2Name && $o1Color === $o2Color) {
        return 0;
    }

    if (self::CARD_COLORS[$o1Color] >= self::CARD_COLORS[$o2Color]) {
        return (self::CARD_NAMES[$o1Name] >= self::CARD_NAMES[$o2Name]) ? +1 : -1;
    }

    /* Ex :
     * carte 1 == 3 de carreau
     * carte 2 == 3 de pique
     * Une fois ici c'est que la couleur de la carte 1 est forcément < a la couleur de la carte 2
     * Comme les noms sont égaux, la deuxième carte est forcément supérieur , on return -1
     */
     return (self::CARD_NAMES[$o1Name] > self::CARD_NAMES[$o2Name]) ? +1 : -1;
}
----

==== Les différents "tests" permettant de tester cette méthode.

[, php]
----
public function testCompareEqualityOfCards()
{
    // 0 si les deux cartes sont considérées comme égales
    $o1 = new Card('As', 'Trèfle');
    $o2 = new Card('as', 'Trèfle');
    $this->assertEquals(0, Card::compare($o1, $o2));
}

public function testCompareSecondSuperiorToFirst()
{
    // -1 si $o1 est considérée inférieur à $o2
    $o1 = new Card('3', 'pique');
    $o2 = new Card('as', 'coeur');
    $this->assertEquals(-1, Card::compare($o1, $o2));

    $o1 = new Card('as', 'trèfle');
    $o2 = new Card('as', 'pique');
    $this->assertEquals(-1, Card::compare($o1, $o2));
}

public function testCompareFirstSuperiorToSecond()
{
    // +1 si $o1 est considérée supérieur à $o2
    $o1 = new Card('roi', 'pique');
    $o2 = new Card('valet', 'coeur');
    $this->assertEquals(+1, Card::compare($o1, $o2));

    $o1 = new Card('3', 'pique');
    $o2 = new Card('3', 'coeur');
    $this->assertEquals(+1, Card::compare($o1, $o2));
}
----

==== La méthode static `gameOf52Cards`.

Une méthode qui retourne une liste contenant 52 cartes (Celle-ci sera utile dans la `Guess`).

[, php]
----
/**
 * @return ArrayCollection tableau contenant 52 cartes
 */
public static function gameOf52Cards(): ArrayCollection
{
    $allCards = new ArrayCollection();

    foreach (self::CARD_COLORS as $ccKey => $ccValue) {
        foreach (self::CARD_NAMES as $cnKey => $cnValue) {
            $allCards->add(new Card($cnKey, $ccKey));
        }
    }

    return $allCards;
}
----

==== Les différents "tests" permettant de tester cette méthode.

[, php]
----
public function testGameOf52CardsNumbersCards()
{
    // Nombre total de cartes
    $this->assertEquals(52, Card::gameOf52Cards()->count());
}

public function testGameOf52CardsFirstCard()
{
    // verifie la premiere carte
    $testFirstCard = new Card('as', 'pique');
    $this->assertEquals(0, card::compare(Card::gameOf52Cards()->first(), $testFirstCard));
}

public function testGameOf52CardsLastCard()
{
    // verifie la dernière carte
    $testLastCard = new Card('2', 'trèfle');
    $this->assertEquals(0, card::compare(Card::gameOf52Cards()->last(), $testLastCard));
}
----

=== La classe Guess (Localisation : `src/Core/Guess.php`) :

* Cette classe est responsable de la logique du jeu.
Ses méthodes sont également testées dans `GuessTest` (Localisation : `tests/Core/GuessTest.php`).

[, php]
----
<?php

namespace App\Core;

use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Guess : la logique du jeu.
 * @package App\Core
 */
class Guess
{
  /**
   * @var $cards ArrayCollection a array of Cards
   */
  private $cards;

  /**
   * @var $help bool Si le joueur active l'aide ou non
   */
  private $help;

  /**
   * @var $selectedCard Card This is the card to be guessed by the player
   */
  private $selectedCard;

  /**
   * @var $submissions int nombre de fois où le joueur a soumis une carte
   */
  private $submissions;

  /**
   * @var $verifiedCards ArrayCollection la liste des cartes vérifier
   */
  private $verifiedCards;

  /**
   * Guess constructor.
   * @param int $idGame choix du jeu de cartes
   * @param bool $help Demande d'aide
   * @param bool $defaultCardForTest prendre la carte par défaut pour les tests
   */
  public function __construct(int $idGame = 52, bool $help = false, bool $defaultCardForTest = false)
  {
      $this->cards = ($idGame === 52) ? Card::gameOf52Cards() : Card::gameOf52Cards();
      $this->help = $help;
      $this->selectedCard = ($defaultCardForTest) ? new Card("roi", "pique") : $this->cards->get(mt_rand(0, $this->cards->count() - 1));
      $this->submissions = 0;
      $this->verifiedCards = new ArrayCollection();
  }
----

==== La méthode `verify`.

Elle prend en paramètre la carte à tester par le joueur et vérifie si elle correspond à la carte à deviner.

Retourne :

* O si ce n'est pas la bonne carte.
* 1 si c'est la bonne carte.

Et si l'aide est activée :

* -1 si la carte soumise est plus petite que celle à deviner.
* +1 si la carte soumise est plus grande que celle à deviner.

[, php]
----

public function verify(Card $card): int
{
    $this->submissions++;
    $this->verifiedCards->add($card);

    if ($this->help) {
        return (Card::compare($card, $this->selectedCard) === 0) ? 1 : Card::compare($card, $this->selectedCard);
    }

    return (Card::compare($card, $this->selectedCard) === 0) ? 1 : 0;
}
----

==== Les différents "tests" permettant de tester cette méthode.

[, php]
----
public function testVerifyWithHelp()
{
    // carte par défault pour test => roi de pique
    $guess = new Guess(52, true, true);

    // -1 si la carte qu'il a soumise est plus petite que celle à deviner.
    $this->assertEquals(-1, $guess->verify(new Card("2", "coeur")));

    // +1 si la carte qu'il a soumise est plus grande que celle à deviner.
    $this->assertEquals(+1, $guess->verify(new Card("as", "pique")));
}

public function testVerifyWithoutHelp()
{
    // carte par défault pour test => roi de pique

    $guess = new Guess(52, false, true);

    // O si ce n'est pas la bonne carte
    $this->assertEquals(0, $guess->verify(new Card("3", "trèfle")));

    // 1 si c'est la bonne carte
    $this->assertEquals(1, $guess->verify(new Card("roi", "pique")));
}
----

==== La méthode `stats`.

Elle retourne des éléments d'analyse de la partie.

[, php]

----
public function stats(): string
{
    $strategy = null;

    if ($this->submissions === 1) {
        $strategy = "Bravo, vous l'avez trouvé du premier coup, vous êtes chanceux.";
    }

    if ($this->submissions > 1 && $this->submissions <= $this->cards->count()) {
        $note = 0;
        $recupCard = $this->verifiedCards->first();

        foreach ($this->verifiedCards as $vc) {
            if ($vc->getName() === $recupCard->getName() || $vc->getColor() === $recupCard->getColor()) {
                $note++;
            } else {
                $note--;
            }
            $recupCard = $vc;
        }

        $strategy = ($note > 0) ?
            "Bravo, vous avez trouvé en testant les cartes par nom/couleur, une bonne stratégie." :
            "Bravo, mais vous êtes chanceux, car vous avez trouvé en testant les cartes un peu aléatoirement.";
    }

    if ($this->submissions > $this->cards->count()) {
        $strategy = "Bravo, vous avez trouvé. Mais vous effectuez plus d'essais que de carte dans le jeu. Certaines cartes ont été testées plusieurs fois.";
    }

    return $strategy . " Nombre de soumission(s) : " . $this->getSubmissions();
}
----

==== Les différents "tests" permettant de tester cette méthode.

[, php]

----
public function testStatsDirectFound()
{
    // carte à deviner == Roi -> Pique
    $guess = new Guess(52, false, true);
    $guess->verify(new Card("roi", "pique"));
    $this->assertEquals("Bravo, vous avez trouvé du premier coup, vous êtes un chanceux. Nombre de soumission(s) : 1", $guess->stats());
}


public function testStatsWithGoodStrategy()
{
    // carte à deviner == Roi -> Pique
    $guess = new Guess(52, false, true);
    $guess->verify(new Card("As", "carreau"));
    $guess->verify(new Card("As", "coeur"));
    $guess->verify(new Card("As", "trèfle"));
    $guess->verify(new Card("As", "pique"));
    $guess->verify(new Card("roi", "carreau"));
    $guess->verify(new Card("roi", "coeur"));
    $guess->verify(new Card("roi", "trèfle"));
    $guess->verify(new Card("roi", "pique"));
    $this->assertEquals("Bravo, vous avez trouvé en testant les cartes par nom/couleur, bonne stratégie. Nombre de soumission(s) : 8", $guess->stats());
}

public function testStatsWithBadStrategy()
{
    // carte à deviner == Roi -> Pique
    $guess = new Guess(52, false, true);
    $guess->verify(new Card("3", "trèfle"));
    $guess->verify(new Card("4", "carreau"));
    $guess->verify(new Card("roi", "pique"));
    $guess->verify(new Card("8", "trèfle"));
    $guess->verify(new Card("6", "trèfle"));
    $guess->verify(new Card("roi", "pique"));
    $this->assertEquals("Bravo, mais vous êtes chanceux, car vous avez trouvé en testant les cartes un peu aléatoirement. Nombre de soumission(s) : 6", $guess->stats());
}

public function testStatsMoreThan52Trials()
{
    // carte à deviner == Roi -> Pique
    $guess = new Guess(52, false, true);

    for ($i = 0; $i < 52; $i++) {
        $guess->verify(new Card("4", "coeur"));
    }
    $guess->verify(new Card("roi", "pique"));

    $this->assertEquals("Bravo, vous avez trouvé. Mais vous effectuez plus d'essais que de carte dans le jeu. Certaines cartes ont été testées plusieurs fois. Nombre de soumission(s) : 53", $guess->stats());
}
----

=== Résultat obtenu suite au lancement des tests unitaires :

----
C:\Symfony\Projects\GuessWhat>php bin/phpunit

PHPUnit 7.5.20 by Sebastian Bergmann and contributors.

Testing Project Test Suite
......................                                            22 / 22 (100%)

Time: 192 ms, Memory: 6.00 MB

OK (22 tests, 29 assertions)
----

== Conclusion :

Au cours de ce projet, nous n'avons eu aucune difficulté majeure.
Sauf lors de la mise en place de la logique du jeu (en particulier la classe `Guess`). +
Nous avons eu des difficultés avec l'approche sur la qualité stratégique d'un joueur et de la mise en place de son algorithme. +
Nous avons finalement pu résoudre ce problème grâce à une première ébauche, encore perfectible, de l'algorithme (méthode `stats`, class `Guess`).

